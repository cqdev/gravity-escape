import * as images from './objects/images';
import { canvas } from './objects/canvas';
import { asteroids } from './objects/asteroids';
import { explosions } from './objects/explosions';
import { createPlanet } from './objects/planet';
import { createGalaxy } from './objects/galaxy';
import { createBlackhole } from './objects/blackhole';
import { createEarth } from './objects/earth';
import { createRocket, bindRocketControls } from './objects/rocket';
import stars from './objects/stars';
import { detectCollision } from './objects/collision-detection';
import { drawStats, drawScore } from './objects/stats';

const boot = () => {

    const viewport = document.getElementById('viewport');
    const startBtn = document.getElementById('start');
    const roster = document.getElementById('roster');
    const shipRoster = document.getElementById('spacecraft-roster');
    const spacecraftImgs = shipRoster.getElementsByTagName('img');
    const scoreEl = document.getElementById('score');
    let spacecraftIndex: number = 0;

    const selectShip = (index: number) => {
        spacecraftIndex = index;
        for (let i = 0; i < spacecraftImgs.length; i++) {
            if (index === i) spacecraftImgs.item(i).classList.add('selected');
            else spacecraftImgs.item(i).classList.remove('selected');
        }
    }

    const bindShipSelectHandlers = () => {
        for (let i = 0; i < spacecraftImgs.length; i++) {
            spacecraftImgs.item(i).onclick = selectShip.bind(null, i);
        }
    }

    // --

    images
        .preload()
        .then(() => {

            bindShipSelectHandlers();

            roster.hidden = false;
            viewport.innerHTML = '';
            viewport.appendChild(canvas.element);

            let loopId: number;
            let timestamp: number = 0;
            let score = 0;

            const stopGame = () => {
                setTimeout(() => {
                    cancelAnimationFrame(loopId);
                    canvas.hide();
                    scoreEl.textContent = `Your score: ${score}`;
                    roster.hidden = false;
                }, 6000);
            }

            const startGame = () => {
                let loopId: number;
                score = 0;

                roster.hidden = true;
                canvas.show();

                const planet = createPlanet();
                const blackhole = createBlackhole();
                const galaxy = createGalaxy();
                const earth = createEarth(timestamp);
                const rocket = createRocket(timestamp, spacecraftIndex);
                const releaseControls = bindRocketControls(rocket);

                asteroids.clear();
                stars.create(100);

                const loop = (t: number) => {
                    timestamp = t;
                    canvas.ctx.clearRect(0, 0, canvas.width, canvas.height);
                    asteroids.updatePositions(timestamp);
                    earth.updatePosition(timestamp);
                    rocket.updatePosition(timestamp);
                    explosions.updatePositions(timestamp);
                    if (!rocket.destroyed) {
                        const points = detectCollision(rocket, asteroids, timestamp);
                        if (points === -1) stopGame();
                        else score += points
                        drawStats(rocket);
                    }
                    else {
                        releaseControls();
                    }
                    drawScore(score);

                    explosions.paint();
                    rocket.paint();
                    asteroids.paint();
                    stars.paint();
                    planet.paint();
                    if (earth.positionX > canvas.width) {
                        earth.positionX > 2000 && earth.reset();
                    }
                    else earth.paint();
                    galaxy.paint();
                    blackhole.paint();

                    loopId = requestAnimationFrame(loop);
                }

                loop(timestamp);
            }

            startBtn.addEventListener('click', startGame);

        })
        .catch(err => {
            alert(err.message);
            console.error(err);
        })
}

window.addEventListener('load', boot);