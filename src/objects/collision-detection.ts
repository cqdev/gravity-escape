import { IParticle } from '../lib/AbstractParticle';
import { Asteroid } from '../lib/Asteroid';
import { INumericProjection } from '../lib/Vector';
import { Asteroids } from '../lib/Asteroids';
import { LaserBeam } from '../lib/LaserBeam';
import { Missile } from '../lib/Missile';
import { Mine } from '../lib/Mine';
import { Rocket } from '../lib/Rocket';
import { canvas } from './canvas';

import { explosions } from './explosions';

// --
const createAsteroidExplosion = (a: Asteroid, timestamp: number) => {
    explosions.create({
        position: {
            x: a.positionX + Math.round(a.width / 2),
            y: a.positionY + Math.round(a.height / 2),
        },
        maxParticles: 50 + Math.round(a.mass),
        minParticleSpeed: 50,
        maxParticleSpeed: 100,
        particleRadiusDecreaseRate: .02 + .001 * a.width,
        timestamp,
    });
}

const createAsteroidLaserExplosion = (a: Asteroid, b: LaserBeam, timestamp: number) => {
    explosions.create({
        position: {
            x: b.positionX,
            y: b.positionY
        },
        maxParticles: 50 + Math.round(a.mass),
        minParticleSpeed: 50,
        maxParticleSpeed: 100,
        particleRadiusDecreaseRate: .02 + .001 * a.width,
        timestamp,
    });
}

const createAsteroidMissileExplosion = (a: Asteroid, m: Missile, timestamp: number) => {
    explosions.create({
        position: {
            x: m.positionX,
            y: m.positionY
        },
        maxParticles: 80 + Math.round(a.mass),
        minParticleSpeed: 50,
        maxParticleSpeed: 120,
        particleRadiusDecreaseRate: .02 + .001 * a.width,
        timestamp,
    });
}

const createAsteroidMineExplosion = (a: Asteroid, m: Mine, timestamp: number) => {
    explosions.create({
        position: {
            x: m.positionX,
            y: m.positionY
        },
        maxParticles: 50 + Math.round(a.mass),
        minParticleSpeed: 70,
        maxParticleSpeed: 100,
        particleRadiusDecreaseRate: .02 + .001 * a.width,
        timestamp,
    });
}

const createRocketExplosion = (r: Rocket, timestamp: number) => {
    explosions.create({
        position: {
            x: r.positionX,
            y: r.positionY
        },
        maxParticles: 500,
        minParticleSpeed: 50,
        maxParticleSpeed: 300,
        particleRadiusDecreaseRate: .02,
        timestamp,
    });
}

const createOutOfPerimeterRocketExplosion = (r: Rocket, x: number, y: number, timestamp: number) => {
    explosions.create({
        position: {
            x,
            y,
        },
        maxParticles: 500,
        minParticleSpeed: 50,
        maxParticleSpeed: 300,
        particleRadiusDecreaseRate: .02,
        timestamp,
    });
}


// --

let missileHitArea: IParticle = null;
const missileHitImpactWidth = 500;
const missileHitImpactHeight = 400;

export const detectCollision = (rocket: Rocket, asteroids: Asteroids, timestamp: number): number => {

    let points = 0;
    let asteroid: Asteroid;


    for (let i = 0; i < asteroids.items.length - 1; i++) {
        asteroid = asteroids.items[i];

        // laser beam collision
        rocket.beams.forEach(beam => {
            if (beam.collide(asteroid)) {
                createAsteroidLaserExplosion(asteroid, beam, timestamp);
                asteroids.remove(asteroid);
                i--;
                rocket.removeBeam(beam);
                ++points;
            }
        });

        if (missileHitArea) {
            if (asteroid.collide(missileHitArea)) {
                createAsteroidExplosion(asteroid, timestamp);
                asteroids.remove(asteroid);
                i--;
                ++points;
            }
        }

        // missile collision
        rocket.missilePod.missiles.forEach(missile => {
            if (missile.collide(asteroid)) {
                createAsteroidMissileExplosion(asteroid, missile, timestamp);
                asteroids.remove(asteroid);
                i--;
                rocket.missilePod.removeMissile(missile);

                missileHitArea = {
                    positionX: missile.positionX - Math.round(missileHitImpactWidth / 2),
                    positionY: missile.positionY - Math.round(missileHitImpactHeight / 2),
                    width: missileHitImpactWidth,
                    height: missileHitImpactHeight,
                }
                ++points;
            }
        });

        // mine collision
        rocket.minePod.mines.forEach(mine => {
            if (mine.collide(asteroid)) {
                createAsteroidMineExplosion(asteroid, mine, timestamp);
                asteroids.remove(asteroid);
                i--;
                rocket.minePod.removeMine(mine);
                ++points;
            }
        });

        // rocket collision
        if (asteroid.collide(rocket)) {
            createRocketExplosion(rocket, timestamp);
            asteroids.remove(asteroid);
            i--;
            rocket.destroy();
            return -1;
        }
    }

    if (rocket.positionX < -rocket.width || rocket.positionX > canvas.width || rocket.positionY < -rocket.height || rocket.positionY > canvas.height) {
        rocket.destroy();
        const x = rocket.positionX > -rocket.width ? rocket.positionX < canvas.width ? rocket.positionX : canvas.width : 0;
        const y = rocket.positionY > -rocket.height ? rocket.positionY < canvas.height ? rocket.positionY : canvas.height : 0;

        createOutOfPerimeterRocketExplosion(rocket, x, y, timestamp);
        return -1;
    }

    missileHitArea = null;

    return points;
}
