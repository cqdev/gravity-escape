import { Earth } from '../lib/Earth';
import { canvas } from './canvas';
import { moon } from './images';

// --

export const createEarth = (timestamp: number) => {
    const e = new Earth({
        angle: 20,
        speed: 46,        
        imageUrl: moon,
        mass: 12,
        position: {
            y: 160,
            x: -5,
        },
        size: {
            width: 8,
            height: 8
        },
        forces: [
            {
                acceleration: 2,
                angle: 270,
                name: 'gravity'
            }
        ],
        timestamp
    }, canvas.ctx);

    return e;
}