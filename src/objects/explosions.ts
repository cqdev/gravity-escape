import { Explosions } from '../lib/Explosions';
import { canvas } from './canvas';
import { gravity } from './gravity';

export const explosions = new Explosions([gravity], canvas);