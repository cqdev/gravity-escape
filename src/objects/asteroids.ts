import { Asteroids } from '../lib/Asteroids';
import { IImageParticleOptions } from '../lib/ImageParticle';
import { Asteroid } from '../lib//Asteroid';
import { GravityField } from '../lib/GravityField';
import { pickFromArray } from '../lib/utils';
import { gravity, gravityField } from './gravity';
import { canvas } from './canvas';
import { random } from '../lib/utils';
import * as images from './images';

// -


export const asteroids = new Asteroids({
    forces: [gravity],
    gravityField,
    images: images.asteroids,
    maxAsteroids: 10,
    maxMass: 20,
    minMass: 4,
    maxSpeed: 10,
    minSpeed: 2,
    maxSize: 50,
    minSize: 30
}, canvas);
