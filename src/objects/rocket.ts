import { Rocket } from '../lib/Rocket';
import { MissilePod } from '../lib/MissilePod';
import { MinePod } from '../lib/MinePod';
import { canvas } from './canvas';
import { pickFromArray } from '../lib/utils';
import { gravityField } from './gravity';
import { rockets, missiles, mines } from './images';


// --

export const createRocket = (timestamp: number, index: number) => {

    const position = {
        y: Math.round(window.innerHeight / 2),
        x: Math.round(window.innerWidth / 2),
    }

    let enginePower: number;
    let horizontalAcceleration: number;
    let brakeDeceleration: number;
    let maxBeams: number;
    let minesCount: number;
    let missilesCount: number;

    switch (index) {

        case 0:
            maxBeams = 10;
            enginePower = 20;
            brakeDeceleration = 25;
            horizontalAcceleration = 20;
            minesCount = 15;
            missilesCount = 10;
            break;

        case 1:
            maxBeams = 8;
            enginePower = 15;
            brakeDeceleration = 20;
            horizontalAcceleration = 15;
            minesCount = 20;
            missilesCount = 30;
            break;

        case 2:
            maxBeams = 3;
            enginePower = 10;
            brakeDeceleration = 15;
            horizontalAcceleration = 10;
            minesCount = 40;
            missilesCount = 15;
            break;

        case 3:
            maxBeams = 15;
            enginePower = 25;
            brakeDeceleration = 30;
            horizontalAcceleration = 25;
            minesCount = 10;
            missilesCount = 5;
            break;
    }

    const rocket = new Rocket({
        angle: 90,
        speed: 46,
        imageUrl: rockets[index],
        mass: 12,
        position: Object.assign({}, position),
        size: {
            width: 70,
            height: 70
        },
        forces: [
            {
                acceleration: 15,
                angle: 270,
                name: 'gravity'
            }
        ],
        enginePower,
        brakeDeceleration,
        horizontalAcceleration,
        maxBeams,
        timestamp
    }, canvas);

    rocket.addGravityField(gravityField);


    const missilePod = new MissilePod({
        count: missilesCount,
        fireInterval: 2000,
        imageUrl: missiles[0],
        life: 300
    }, canvas.ctx);

    const minePod = new MinePod({
        count: minesCount,
        fireInterval: 2000,
        imageUrl: mines[0],
        life: 300
    }, canvas.ctx);

    rocket.attachMissilePod(missilePod);
    rocket.attachMinePod(minePod);

    return rocket;
}


const keydownHandler = (rocket: Rocket, ev: KeyboardEvent) => {

    console.log(ev.keyCode)

    switch (ev.keyCode) {
        case 38:
            rocket.increaseThrust();
            break;

        case 40:
            rocket.decreaseThrust();
            break;

        case 37:
            rocket.moveLeft();
            break;

        case 39:
            rocket.moveRight();
            break;

        case 66:
            rocket.startBrake();
            break;

        case 32:
            rocket.fire();
            break;

        case 86:
            rocket.missilePod && rocket.missilePod.fire();
            break;

        case 78:
            rocket.minePod && rocket.minePod.fire();
            break;
    }
}

const keyupHandler = (rocket: Rocket, ev: KeyboardEvent) => {

    switch (ev.keyCode) {
        case 38:
            rocket.stopIncreaseThrust();
            break;

        case 40:
            rocket.stopDecreaseThrust();
            break;

        case 37:
            rocket.stopMoveLeft();
            break;

        case 66:
            rocket.stopBrake();
            break;

        case 39:
            rocket.stopMoveRight();
            break;
    }
}

export const bindRocketControls = (rocket: Rocket) => {
    const keydown = keydownHandler.bind(null, rocket);
    const keyup = keyupHandler.bind(null, rocket);
    window.addEventListener('keydown', keydown);
    window.addEventListener('keyup', keyup);

    return () => {
        window.removeEventListener('keydown', keydown);
        window.removeEventListener('keyup', keyup);
    }
}

/*
export const missilePod = new MissilePod({
    count: 50,
    fireInterval: 2000,
    imageUrl: missiles[0],
    life: 300
}, canvas.ctx);


export const minePod = new MinePod({
    count: 15,
    fireInterval: 2000,
    imageUrl: mines[0],
    life: 300
}, canvas.ctx);
*/