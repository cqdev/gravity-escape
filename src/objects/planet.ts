import { RotatingPlanet } from '../lib/RotatingPlanet';
import { canvas } from './canvas';
import { planet } from './images'

// --

export const createPlanet = () => {
    return new RotatingPlanet(planet, 0, window.innerHeight - 300, 800, 800, .02, canvas);
}