import { Rocket } from '../lib/Rocket';
import { canvas } from './canvas';

// --

export const drawStats = (rocket: Rocket) => {
    //canvas.ctx.moveTo(canvas.width - 200, 5);
    canvas.ctx.fillStyle = '#ffb600';
    canvas.ctx.fillRect(canvas.width - 200, 5, 20 * rocket.thrust, 10);
    canvas.ctx.fillStyle = '#ff1900';
    canvas.ctx.fillRect(canvas.width - 200, 20, 2 * rocket.laserState, 10);
    canvas.ctx.fillStyle = '#fff';
    canvas.ctx.font = '14px arial';
    canvas.ctx.fillText(`Thrust: ${rocket.thrust * 20}%`, canvas.width - 300, 14);
    canvas.ctx.fillText(`Laser: ${rocket.beamsLeft}`, canvas.width - 300, 32);
    canvas.ctx.fillText(`Misiles: ${rocket.missilePod.missilesLeft}`, canvas.width - 300, 50);
    canvas.ctx.fillText(`Mines: ${rocket.minePod.minesLeft}`, canvas.width - 300, 68);
}

export const drawScore = (score: number) => {
    canvas.ctx.fillStyle = '#fff';
    canvas.ctx.font = '14px arial';
    canvas.ctx.fillText(`Score: ${score}`, canvas.width - 400, 14);
}