import { Planet } from '../lib/Planet';
import { canvas } from './canvas';
import { galaxy } from './images';

// --

export const createGalaxy = () => {
    return new Planet(galaxy, window.innerWidth - 500, -300, null, null, canvas.ctx);
}