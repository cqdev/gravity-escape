import { Star } from '../lib/Star';
import { canvas } from './canvas';

// --

const stars: Star[] = [];

const create = (count = 100) => {
    for (let i = 0; i < count; i++) {
        stars.push(new Star(Math.random() * canvas.width, Math.random() * canvas.height * 2/3, canvas.ctx));
    }
}

const paint = () => {
    stars.forEach(star => star.paint());
}

export default {
    create,
    paint
}