import { GravityField } from '../lib/GravityField';
import { Vector } from '../lib/Vector';
import { Particle } from '../lib/Particle';

// --

export const gravity = new Vector({
    acceleration: 20,
    angle: 270,
    name: 'gravity'
});

// --

const fakePlanet = new Particle({
    mass: 50,
    position: {
        x: 150,
        y: 150
    },
    speed: 0
})

export const gravityField = new GravityField(fakePlanet, 3);