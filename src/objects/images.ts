import * as Promise from 'bluebird';

// --

export const loadImage = (src: string): Promise<void> => {
    console.log('Preloading', src);
    return new Promise<void>((resolve, reject) => {
        if (typeof src !== 'string' || !src) reject(new TypeError('Source must be a string'));
        const img = new Image();
        img.src = src;

        img.onload = () => {
            resolve(void 0);
        };

        img.onerror = (err) => {
            console.warn(`Failed to load image ${src}`);
            console.error(err);
            reject(err);
        };
    });
}

// --

/*
    //'https://pbs.twimg.com/profile_images/3627005607/2aae0e25c02fe03e8b22437d69d6802c.png',
    //'https://cdn.uanews.arizona.edu/s3fs-public/styles/blog_image_large_600px_w/public/story-images/RQ36%20white%20backgr.png?6fLOyOxROtXyTUa_wWTEeGKhWn5n9ycP=&itok=WJZWjJgL',
    //'http://ecx.images-amazon.com/images/I/71xwHBvIB8L.png',
    //'https://media.nationalgeographic.org/assets/interactives/433/90b/43390b12-c7c2-450e-94de-ac2e04b52359/public/splash/images/asteroid.png',
    //'https://vignette2.wikia.nocookie.net/bsgoguide/images/d/da/White_Asteroid.png/revision/latest?cb=20150108172853',
    //'http://lockheedmartin.com/generationbeyond/solarsystem/images/planet-trans-asteroid.png',
    */

export const asteroids = [
    './images/rock1.png',
    './images/rock2.png',
    './images/rock3.png',
    './images/rock4.png',
    './images/rock5.png'
];

export const rockets = [
    './images/spacecraft1.png',
    './images/spacecraft2.png',
    './images/spacecraft3.png',
    './images/spacecraft4.png',
];


export const moon = './images/moon.png';
export const blackhole = './images/blackhole.png';
export const galaxy = './images/galaxy.png';
export const planet = './images/planet.png';
export const smoke = './images/smoke.png';

export const missiles = [
    './images/missile1.png'
];

export const mines = [
    './images/mine2.png'
];
// --

export const preload = () => {
    const a = asteroids.slice(0).concat(rockets).concat(missiles).concat(mines);
    a.push(moon, blackhole, galaxy, planet, smoke);

    return Promise.all(a.map(s => loadImage(s)));
}