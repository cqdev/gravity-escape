import { Planet } from '../lib/Planet';
import { RotatingPlanet } from '../lib/RotatingPlanet';
import { canvas } from './canvas';
import { blackhole } from './images';

export const createBlackhole = () => {
    return new RotatingPlanet(blackhole, 50, 50, 200, 200, .1, canvas);
}