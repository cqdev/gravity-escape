import { Explosion, IExplosionOptions } from './Explosion';
import { Canvas } from './Canvas';
import { Vector, IVectorOptions } from './Vector';


/**
 * Explosions
 */
export class Explosions {

    protected _items: Explosion[] = [];

    constructor(protected forces: (Vector | IVectorOptions)[], protected _canvas: Canvas) {

    }

    public create(o: IExplosionOptions) {
        const e = new Explosion(Object.assign({}, o, { onDone: this.onExplosionDone.bind(this) }), this._canvas);
        this._items.push(e);
    }

    protected onExplosionDone(e: Explosion) {
        const index = this._items.indexOf(e);
        this._items.splice(index, 1);
    }

    public updatePositions(t: number) {
        this._items.forEach(e => e.updatePositions(t));
    }

    public paint() {
        this._items.forEach(e => e.paint());
    }
}