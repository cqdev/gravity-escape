export const roundNumber = function (number: number, decimals: number = 5) {
    return Math.round(number * Math.pow(10, decimals)) / Math.pow(10, decimals);
}

export const toDegrees = function (angle: number) {
    return angle * (180 / Math.PI);
}

export const toRadians = function (angle: number) {
    return angle * (Math.PI / 180);
}

export const pickFromArray = function <T>(arr: T[]): T {
    return arr[Math.floor(Math.random() * arr.length)];
}

export const random = (max: number, min?: number, round?: boolean) => {
    let r: number;
    if (min === undefined) {
        r = Math.random() * max;
    }
    else r = Math.random() * (max - min) + min;

    if (round === true) r = Math.round(r);
    return r;
}