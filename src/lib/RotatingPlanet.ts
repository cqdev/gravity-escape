import { Canvas } from './Canvas';
import { toRadians } from './utils';

/**
 * RotatingPlanet
 */
export class RotatingPlanet {

    protected _element: HTMLImageElement;
    protected _angle: number;

    constructor(protected _imageUrl: string, protected _left: number, protected _top: number, protected _width: number, protected _height: number, protected _speed: number, private _canvas: Canvas) {
        this._element = new Image();
        this._element.src = _imageUrl;
        this._element.width = _width;
        this._element.height = _height;
        this._angle = 0;
    }

    public paint() {
        this._angle += this._speed;
        this._angle < 360 || (this._angle = 0);
        this._canvas.ctx.save();
        this._canvas.ctx.translate(this._left + this._element.width / 2, this._top + this._element.width / 2);
        this._canvas.ctx.rotate(toRadians(this._angle));
        this._canvas.ctx.drawImage(this._element, -this._element.width / 2, -this._element.height / 2, this._element.width, this._element.height);
        this._canvas.ctx.restore();
    }
}