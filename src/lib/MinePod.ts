import { Mine, IMineOpts } from './Mine';
import { Rocket } from './Rocket';

// --

export interface IMinePodOpts {
    imageUrl: string;
    count: number;
    life: number;
    fireInterval: number;
}

/**
 * MinePod
 */
export class MinePod {

    protected _mines: Mine[] = [];
    protected _count: number;
    protected _fired: number;
    protected _fireInterval: number;
    public owner: Rocket = null;
    protected _lastFired: number = 0;
    protected _imageUrl: string;

    constructor(opts: IMinePodOpts, protected _ctx: CanvasRenderingContext2D) {
        this._count = isNaN(opts.count) ? 99999 : opts.count;
        this._fireInterval = isNaN(opts.fireInterval) ? 2000 : opts.fireInterval;
        this._imageUrl = opts.imageUrl;

        this.reset();
    }

    public get mines() {
        return this._mines;
    }

    public reset() {
        this._fired = 0;
        this._lastFired = 0;
        this.clear();
    }

    public fire() {

        if (this.minesLeft === 0 || this._fireInterval > this.owner.timestamp - this._lastFired) return;

        const angles = [130, 110, 90, 70, 50];
        this.minesLeft < angles.length && angles.splice(this.minesLeft, angles.length - 1);
        const mines = angles.map((angle, i) => {
            return new Mine({
                imageUrl: this._imageUrl,
                angle,
                speed: 20,
                mass: 5,
                position: {
                    x: this.owner.positionX + this.owner.width,
                    y: this.owner.positionY
                },
                size: {
                    height: 10,
                    width: 20,
                },
                timestamp: this.owner.timestamp,
            }, this._ctx)
        });
        this._mines.push(...mines);
        this._fired += mines.length;
        this._lastFired = this.owner.timestamp;
    }

    public get minesLeft() {
        return this._count - this._fired;
    }

    public removeMine(what: number): void;
    public removeMine(what: Mine): void;

    public removeMine(what: number | Mine) {
        if (what instanceof Mine) what = this._mines.indexOf(what);
        this._mines.splice(what, 1);
    }

    public updatePosition(timestamp: number) {
        for (let i = 0; i < this._mines.length; i++) {
            if (this._mines[i].positionY < 0) {
                this.removeMine(i);
                i--;
            }
            else this._mines[i].updatePosition(timestamp);
        }
    }

    public paint() {
        this._mines.forEach(m => m.paint());
    }

    public clear() {
        this._mines.length = 0;
    }
}