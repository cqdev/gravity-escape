import { ImageParticle, IImageParticleOptions } from './ImageParticle';
import { FlameParticle, SmokeParticle } from './Rocket';
import { random } from './utils';

/**
 * MissileSmokeParticle
 */
export class MissileSmokeParticle extends SmokeParticle {

    public paint() {
        this._ctx.fillStyle = `rgba(255, 255, 255, ${1 / (this.life / 7)})`;
        this._ctx.beginPath();
        this._ctx.arc(this.positionX, this.positionY, 1, 0, 2 * Math.PI);
        this._ctx.fill();
    }
}

// --

export interface IMissileOpts extends IImageParticleOptions {

}

/**
 * Missile
 */
export class Missile extends ImageParticle {

    public life: number = 0;
    protected _flames: FlameParticle[] = [];
    protected _max: number = 30;
    protected _fspeed: number = .5;
    protected _fsize: number = 2;
    protected _trailSmoke: MissileSmokeParticle[] = [];
    protected _smokeLife: number = 500;
    protected _smokeOffsetX: number;
    protected _smokeOffsetY: number;    

    protected init() {
        this._smokeOffsetX = Math.round(this.width / 2);
        this._smokeOffsetY = this.height + 14; 
    }

    protected paintFlames() {
        this._ctx.globalCompositeOperation = "lighter";

        for (let i = 0; i < 10; i++) {
            let p = new FlameParticle(this.positionX + this._smokeOffsetX, this.positionY + this.height + 4, (Math.random() * 2 * this._fspeed - this._fspeed) / 2, 0 - Math.random() * 2 * this._fspeed);
            this._flames.push(p);
        }

        for (let i = 0; i < this._flames.length; i++) {
            this._ctx.fillStyle = "rgba(" + (260 - (this._flames[i].life * 2)) + "," + ((this._flames[i].life * 2) + 50) + "," + (this._flames[i].life * 2) + "," + (((this._max - this._flames[i].life) / this._max) * 0.4) + ")";

            this._ctx.beginPath();

            this._ctx.arc(this._flames[i].x, this._flames[i].y, (this._max - this._flames[i].life) / this._max * (this._fsize / 2) + (this._fsize / 2), 0, 2 * Math.PI);
            this._ctx.fill();

            this._flames[i].x += this._flames[i].xs;
            this._flames[i].y -= this._flames[i].ys;

            this._flames[i].life++;

            if (this._flames[i].life >= this._max) {
                this._flames.splice(i, 1);
                i--;
            }
        }
    }

    protected paintSmokeTrail() {

        this._trailSmoke.push(new MissileSmokeParticle({
            angle: random(280, 260, true),
            speed: 60,
            position: {
                x: this.positionX + this._smokeOffsetX,
                y: this.positionY + this._smokeOffsetY
            },
            size: {
                width: 3,
                height: 3
            },
            timestamp: this._timestamp
        }, this._ctx));

        for (let i = 0; i < this._trailSmoke.length; i++) {
            if (this._trailSmoke[i].life > this._smokeLife) {
                this._trailSmoke.splice(i, 1);
                i--;
            }
            else {
                this._trailSmoke[i].paint();
            }
        }
    }

    public updatePosition(timestamp: number) {
        ++this.life;
        super.updatePosition(timestamp);
        this._trailSmoke.forEach(s => s.updatePosition(timestamp));
    }

    public paint() {
        this._ctx.globalCompositeOperation = 'destination-over';
        super.paint();
        this.paintSmokeTrail();
        this.paintFlames();
        this._ctx.globalCompositeOperation = 'destination-over';
    }
}

