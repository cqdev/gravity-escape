import { AbstractParticle, IAbstractParticleOptions } from './AbstractParticle';
import { Rocket } from './Rocket';


/**
 * LaserBeam
 */
export class LaserBeam extends AbstractParticle {

    protected _color = '#89f442';
    protected _life = 0;

    constructor(protected _owner: Rocket, options: IAbstractParticleOptions) {
        super(options);
    }

    public updatePosition(t: number) {
        if (this._life > 400) {
            this._owner.removeBeam(this);
        }
        else super.updatePosition(t);
    }

    public paint() {
        ++this._life;
        this._owner.ctx.fillStyle = this._color;
        this._owner.ctx.beginPath();
        this._owner.ctx.moveTo(this.positionX, this.positionY);
        this._owner.ctx.fillRect(this.positionX - this.width / 2, this.positionY, this.width/2, this.height);

    }
}