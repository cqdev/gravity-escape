import { pickFromArray } from './utils';

// --

const colors = ['#fff', '#aacbff', '#fff5aa', '#ff3535'];

/**
 * Star
 */
export class Star {

    protected _color: string;

    constructor(protected _positionX: number, protected _positionY: number, private _ctx: CanvasRenderingContext2D) {
        this._color = pickFromArray(colors);
    }

    public paint() {
        this._ctx.fillStyle = this._color;
        var d = Math.random() > .98 ? 2 : 1;    // emulate flicker effect
        this._ctx.fillRect(this._positionX, this._positionY, d, d);
    }
}