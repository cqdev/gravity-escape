import { INumericProjection, IVectorOptions, Vector } from './Vector';
import { Vectors } from './Vectors';
import { toRadians, roundNumber } from './utils';


// --

export interface IMovableOptions {
    forces?: IVectorOptions[];
    position?: INumericProjection;
    speed?: number;
    angle?: number;
    timestamp?: number;
}

// --

export interface IPositionable {
    positionX: number;
    positionY: number;
}

/**
 * Movable
 */
export class Movable extends Vectors implements IPositionable {

    protected _position: INumericProjection;
    protected _velocity: INumericProjection;
    protected _timestamp: number;
    protected _defaultSpeed: number;
    protected _defaultAngle: number;
    protected _defaultPosition: INumericProjection;    
    

    constructor(opts: IMovableOptions) {
        typeof opts === 'object' && opts || (opts = {});
        Array.isArray(opts.forces) || (opts.forces = []);
        super(...opts.forces);
        typeof opts.position === 'object' && opts.position || (opts.position = {} as INumericProjection);
        this._timestamp = Number.isNaN(opts.timestamp) || opts.timestamp < 0 ? 0 : opts.timestamp;
        this._defaultAngle = Number.isNaN(opts.angle) || opts.angle < 0 || opts.angle > 360 ? 270 : opts.angle;
        this._defaultPosition = opts.position;
        this._defaultSpeed = Number.isNaN(opts.speed) || opts.speed < 0 ? 0 : opts.speed;

        this.reset();
    }

    public reset() {
        this._position = {
            x: Number.isNaN(this._defaultPosition.x) ? 0 : this._defaultPosition.x,
            y: Number.isNaN(this._defaultPosition.x) ? 0 : this._defaultPosition.y
        }

        const r = toRadians(this._defaultAngle);

        this._velocity = {
            x: roundNumber(Math.cos(r) * this._defaultSpeed, 2),
            y: roundNumber(Math.sin(r) * this._defaultSpeed, 2)
        }
    }

    public get positionX() {
        return Math.round(this._position.x);
    }

    public get positionY() {
        return Math.round(this._position.y);
    }

    public get velocityX() {
        return this._velocity.x;
    }

    public get velocityY() {
        return this._velocity.y;
    }

    public updatePosition(t: number) {
        let dt = (t - this._timestamp) / 1000;
        const resultant = this.resultant;

        this._velocity.x += resultant.x * dt;
        this._velocity.y += resultant.y * dt;

        this._position.x += this._velocity.x * dt;
        this._position.y -= this._velocity.y * dt;

        this._timestamp = t;
    }
}