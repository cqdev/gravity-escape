
import { AbstractParticle } from './AbstractParticle';
import { INumericProjection } from './Vector';

/**
 * GravityField
 */
export class GravityField {

    constructor(protected _source: AbstractParticle, protected _constant = 2) {

    }

    public getAcceleration(target: AbstractParticle): INumericProjection {
        const dx = target.positionX - this._source.positionX;
        const dy = target.positionY - this._source.positionY;

        if (this._source.collide(target)) return {
            x: 99999999,
            y: 99999999
        }

        const d = Math.pow(dx, 2) + Math.pow(dy, 2);
        const D = Math.sqrt(d);

        const g = this._constant * target.mass * this._source.mass / d;

        return {
            x: -g * dx / D,
            y: -g * dy / D
        }
    }

}