import { Vector, IVectorOptions, INumericProjection } from './Vector';

/**
 * Vectors
 */
export class Vectors {

    protected _vectors: Map<string, Vector>;
    protected _resultant: INumericProjection;

    constructor(...forces: IVectorOptions[]) {
        this._vectors = new Map();
        Array.isArray(forces) && forces.forEach(f => this._vectors.set(f.name, new Vector(f)));
    }

    public clearForces() {
        return this._vectors.clear();
    }

    public removeForce(name: string) {
        if (this._vectors.has(name)) {
            this._vectors.delete(name);
            this._resultant = null;
        }
    }

    public addForce(f: IVectorOptions | Vector) {
        if (f instanceof Vector) this._vectors.set(f.name, f);
        else this._vectors.set(f.name, new Vector(f));
        this._resultant = null;
    }

    protected get resultant() {
        if (this._resultant) return this._resultant;

        this._resultant = { x: 0, y: 0 };

        for (let [key, force] of this._vectors) {
            this._resultant.x += force.accelerationX;
            this._resultant.y += force.accelerationY;
        }

        return this._resultant;
    }

    public get forceX() {
        return this.resultant.x;
    }

    public get forceY() {
        return this.resultant.y;
    }
}