import { ImageParticle, IImageParticleOptions } from './ImageParticle';
import { AbstractParticle, IAbstractParticleOptions } from './AbstractParticle';
import { Canvas } from './Canvas';
import { LaserBeam } from './LaserBeam';
import { MissilePod } from './MissilePod';
import { MinePod } from './MinePod';
import { random } from './utils';

// --

export interface IRocketOptions extends IImageParticleOptions {
    enginePower?: number;
    brakeDeceleration?: number;
    horizontalAcceleration?: number;
    maxBeams?: number;
}

/**
 * Rocket
 */
export class Rocket extends ImageParticle {

    protected _flames: FlameParticle[] = [];
    protected _max = 30;
    protected _fspeed = 3;
    protected _fsize = 3;
    protected _thrust = 0;
    protected _maxThrust = 5;
    protected _enginePower: number;
    protected _thrustIncreaseInterval: number = null;
    protected _thrustDecreaseInterval: number = null;
    protected _horizontalAcceleration: number;
    protected _brakeDeceleration: number;
    protected _moveLeft = false;
    protected _moveRight = false;
    protected _brake = false;
    protected _smoke: SmokeParticle[] = [];
    protected _brakeSmoke: SmokeParticle[] = [];
    protected _maxSmoke: number = 200;
    protected _smokeLife: number = 100;
    public beams: LaserBeam[] = [];
    protected _maxBeams = 7;
    protected _bspeed = 160;
    protected _destroyed = false;
    protected _missilePod: MissilePod;
    protected _minePod: MinePod;
    

    constructor(opts: IRocketOptions, protected _canvas: Canvas) {
        super(Object.assign({}, opts), _canvas.ctx);

        this._enginePower = isNaN(opts.enginePower) ? 5 : opts.enginePower;
        this._brakeDeceleration = isNaN(opts.brakeDeceleration) ? 30 : opts.brakeDeceleration;
        this._horizontalAcceleration = isNaN(opts.horizontalAcceleration) ? 20 : opts.horizontalAcceleration;
        this._maxBeams = isNaN(opts.maxBeams) ? 7 : opts.maxBeams;
    }

    public get thrust() {
        return this._thrust;
    }

    public get timestamp() {
        return this._timestamp;
    }

    public get laserState() {
        return Math.round(100 * (this._maxBeams - this.beams.length) / this._maxBeams);
    }

    public get beamsLeft() {
        return this._maxBeams - this.beams.length;
    }

    public get missilePod() {
        return this._missilePod;
    }

    public attachMissilePod(pod: MissilePod) {
        this._missilePod = pod;
        pod.owner = this;
    }

    public get minePod() {
        return this._minePod;
    }

    public attachMinePod(pod: MinePod) {
        this._minePod = pod;
        pod.owner = this;
    }

    public set thrust(val: number) {
        if (val > this._maxThrust || val < 0) return;
        this._thrust = val;
        switch (val) {

            case 1:
                this._fspeed = .5;
                this._fsize = 2;
                break;

            case 1:
                this._fspeed = 1;
                this._fsize = 3;
                break;

            case 2:
                this._fspeed = 2;
                this._fsize = 4;
                break;

            case 3:
                this._fspeed = 3;
                this._fsize = 5;
                break;

            case 4:
                this._fspeed = 4;
                this._fsize = 6;
                break;

            case 5:
                this._fspeed = 5;
                this._fsize = 7;
                break;
        }

        this.removeForce('thrust');
        this.addForce({
            name: 'thrust',
            angle: 90,
            acceleration: val * this._enginePower
        });
    }

    public increaseThrust() {
        if (this._thrustIncreaseInterval) return;
        this.stopDecreaseThrust();
        ++this.thrust;
        this._thrustIncreaseInterval = setInterval(() => {
            ++this.thrust;
        }, 1000);
    }

    public stopIncreaseThrust() {
        clearInterval(this._thrustIncreaseInterval);
        this._thrustIncreaseInterval = null
    }

    public decreaseThrust() {
        if (this._thrustDecreaseInterval) return;
        this.stopIncreaseThrust();
        this._flames.length = 0;
        --this.thrust;
        this._thrustDecreaseInterval = setInterval(() => {
            --this.thrust;
        }, 1000);
    }

    public stopDecreaseThrust() {
        clearInterval(this._thrustDecreaseInterval);
        this._thrustDecreaseInterval = null
    }

    public moveLeft() {
        if (this._moveLeft) return;
        this._moveLeft = true;

        this.addForce({
            acceleration: this._horizontalAcceleration,
            angle: 180,
            name: 'left'
        });
    }

    public stopMoveLeft() {
        this.removeForce('left');
        this._moveLeft = false;
    }

    public moveRight() {
        if (this._moveRight) return;
        this._moveRight = true;

        this.addForce({
            acceleration: this._horizontalAcceleration,
            angle: 0,
            name: 'right'
        })
    }

    public stopMoveRight() {
        this.removeForce('right');
        this._moveRight = false;
    }

    public startBrake() {
        if (this._brake) return;
        this._brake = true;

        this.addForce({
            acceleration: this._brakeDeceleration,
            angle: 270,
            name: 'brake'
        })
    }

    public stopBrake() {
        if (!this._brake) return;
        this._brake = false;
        this.removeForce('brake');
    }

    public fire() {
        if (this.beams.length < this._maxBeams) {
            this.beams.push(new LaserBeam(this, {
                angle: 90,
                mass: 10,
                position: {
                    x: this.positionX + this.width / 2,
                    y: this.positionY - 5
                },
                size: {
                    width: 2,
                    height: 10
                },
                speed: this.velocityY + this._bspeed,
                timestamp: this._timestamp
            }))
        }
    }

    protected paintEngineFlames() {
        this._ctx.globalCompositeOperation = "lighter";

        for (let i = 0; i < 10; i++) {
            let p = new FlameParticle(this.positionX + this.width / 2, this.positionY + this.height + 20, (Math.random() * 2 * this._fspeed - this._fspeed) / 2, 0 - Math.random() * 2 * this._fspeed);
            this._flames.push(p);
        }

        for (let i = 0; i < this._flames.length; i++) {
            this._ctx.fillStyle = "rgba(" + (260 - (this._flames[i].life * 2)) + "," + ((this._flames[i].life * 2) + 50) + "," + (this._flames[i].life * 2) + "," + (((this._max - this._flames[i].life) / this._max) * 0.4) + ")";

            this._ctx.beginPath();

            this._ctx.arc(this._flames[i].x, this._flames[i].y, (this._max - this._flames[i].life) / this._max * (this._fsize / 2) + (this._fsize / 2), 0, 2 * Math.PI);
            this._ctx.fill();

            this._flames[i].x += this._flames[i].xs;
            this._flames[i].y -= this._flames[i].ys;

            this._flames[i].life++;

            if (this._flames[i].life >= this._max) {
                this._flames.splice(i, 1);
                i--;
            }
        }
    }

    protected paintLeftSmoke() {
        this._smoke.forEach(s => s.paint());

        this._moveRight && this._smoke.push(new SmokeParticle({
            angle: random(50, 0, true) + 130,
            speed: 40 + Math.abs(this.velocityX),
            position: {
                x: this.positionX,
                y: this.positionY + Math.round(this.height / 2)
            },
            size: {
                width: 2,
                height: 2
            },
            timestamp: this._timestamp
        }, this._ctx));

        for (let i = 0; i < this._smoke.length; i++) {
            if (this._smoke[i].life > this._smokeLife) {
                this._smoke.splice(i, 1);
                i--;
            }
        }
    }

    protected paintRightSmoke() {
        this._smoke.forEach(s => s.paint());

        this._moveLeft && this._smoke.push(new SmokeParticle({
            angle: random(50, 0, true),
            speed: 40 + Math.abs(this.velocityX),
            position: {
                x: this.positionX + this.width,
                y: this.positionY + Math.round(this.height / 2)
            },
            size: {
                width: 2,
                height: 2
            },
            timestamp: this._timestamp
        }, this._ctx));

        for (let i = 0; i < this._smoke.length; i++) {
            if (this._smoke[i].life > this._smokeLife) {
                this._smoke.splice(i, 1);
                i--;
            }
        }
    }

    protected paintBrakeSmoke() {
        this._brakeSmoke.forEach(s => s.paint());

        this._brake && this._brakeSmoke.push(new SmokeParticle({
            angle: random(120, 60),
            speed: 40 + Math.abs(this.velocityY),
            position: {
                x: this.positionX + Math.round(this.width / 2),
                y: this.positionY - 10
            },
            size: {
                width: 2,
                height: 2
            },
            timestamp: this._timestamp
        }, this._ctx));

        for (let i = 0; i < this._brakeSmoke.length; i++) {
            if (this._brakeSmoke[i].life > this._smokeLife * 1.2) {
                this._brakeSmoke.splice(i, 1);
                i--;
            }
        }
    }

    public removeBeam(b: LaserBeam) {
        const index = this.beams.indexOf(b);
        this.beams.splice(index, 1);
    }

    public updatePosition(t: number) {
        super.updatePosition(t);
        this._smoke.forEach(s => s.updatePosition(t));
        this._brakeSmoke.forEach(s => s.updatePosition(t));
        this.beams.forEach(b => b.updatePosition(t));
        this._missilePod && this._missilePod.updatePosition(t);
        this._minePod && this._minePod.updatePosition(t);                
    }

    public paint() {
        if (!this._destroyed) {
            super.paint();
            this.thrust && this.paintEngineFlames();
        }

        this._ctx.globalCompositeOperation = 'destination-over';
        this.paintRightSmoke();
        this.paintLeftSmoke();
        this.paintBrakeSmoke();

        this.beams.forEach(b => b.paint());
        this._missilePod && this._missilePod.paint();
        this._minePod && this._minePod.paint();        
    }

    public destroy() {
        this._destroyed = true;
    }

    public get destroyed() {
        return this._destroyed;
    }

}

/**
 * FlameParticle
 */
export class FlameParticle {

    public life = 0;

    constructor(public x: number, public y: number, public xs: number, public ys: number) {

    }
}

/**
 * SmokeParticle
 */
export class SmokeParticle extends AbstractParticle {

    public life = 0;

    constructor(options: IAbstractParticleOptions, protected _ctx: CanvasRenderingContext2D) {
        super(options);
    }


    public updatePosition(timestamp: number) {
        ++this.life;
        super.updatePosition(timestamp);
    }

    public paint() {
        this._ctx.fillStyle = `rgba(255, 255, 255, ${1 / (this.life / 2)})`;
        this._ctx.beginPath();
        this._ctx.arc(this.positionX, this.positionY, Math.abs(this.width), 0, 2 * Math.PI);
        this._ctx.fill();
    }
}