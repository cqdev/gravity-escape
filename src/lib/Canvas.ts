
/**
 * Canvas
 */
export class Canvas {

    protected _canvas: HTMLCanvasElement;
    protected _ctx: CanvasRenderingContext2D;

    constructor(protected _width = 800, protected _height = 600) {
        this._canvas = document.createElement('canvas');
        this._canvas.height = _height;
        this._canvas.width = _width;
        this._canvas.className = 'canvas';
        this._canvas.hidden = true;;
        this._ctx = this._canvas.getContext('2d');
        this._ctx.globalCompositeOperation = 'destination-over';
    }

    public get element() {
        return this._canvas;
    }

    public get ctx() {
        return this._ctx;
    }

    public get width() {
        return this._width;
    }

    public get height() {
        return this._height;
    }

    public show() {
        this._canvas.hidden = false;
    }

    public hide() {
        this._canvas.hidden = true;
    }
}