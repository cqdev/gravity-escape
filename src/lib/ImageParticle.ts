import { AbstractParticle, IAbstractParticleOptions } from './AbstractParticle';
import { toRadians } from './utils';

// --

export interface IImageParticleOptions extends IAbstractParticleOptions {
    imageUrl: string;
}

/**
 * ImageParticle
 */
export class ImageParticle extends AbstractParticle {

    protected _element: HTMLImageElement;

    constructor(opts: IImageParticleOptions, protected _ctx: CanvasRenderingContext2D) {
        super(opts);
      
        if (typeof opts.imageUrl !== 'string' || !opts.imageUrl) throw new TypeError('Invalid image url');

        this._element = new Image();
        this._element.src = opts.imageUrl;
        this._element.width = this.width;
        this._element.height = this.height;
        
        this.init();      
    }

    protected init() {
        
    }

    public get element() {
        return this._element;
    }

    public get ctx() {
        return this._ctx;
    }

    public paint() {
        this._ctx.drawImage(this._element, this.positionX, this.positionY, this.width, this.height);    
    }

}