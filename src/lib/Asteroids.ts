import { Asteroid } from './Asteroid';
import { random, pickFromArray } from './utils';
import { Vector } from './Vector';
import { GravityField } from './GravityField';
import { Canvas } from './Canvas';

// --

export interface IAsteroidOptions {
    maxAsteroids: number;
    maxSpeed: number;
    minSpeed: number;
    maxSize: number;
    minSize: number;
    maxMass: number;
    minMass: number;
    forces?: Vector[];
    gravityField?: GravityField;
    images: string[];
}

/**
 * Asteroids
 */
export class Asteroids {

    protected _items: Asteroid[] = [];
    protected _maxAsteroids: number;
    protected _maxSpeed: number;
    protected _minSpeed: number;
    protected _maxSize: number;
    protected _minSize: number;
    protected _minMass: number;
    protected _maxMass: number;
    protected _forces: Vector[];
    protected _gravityField: GravityField;
    protected _images: string[];
    protected _increaseCountRate = 5000;
    protected _cycles = 0;


    constructor(opts: IAsteroidOptions, protected _canvas: Canvas) {
        this._maxAsteroids = opts.maxAsteroids;
        this._maxSpeed = opts.maxSpeed;
        this._minSpeed = opts.minSpeed;
        this._maxSize = opts.maxSize;
        this._minSize = opts.minSize;
        this._minMass = opts.minMass;
        this._maxMass = opts.maxMass;
        this._forces = Array.isArray(opts.forces) ? opts.forces : [];
        this._gravityField = opts.gravityField;
        this._images = opts.images;
    }

    public get items() {
        return this._items;
    }

    public set maxAsteroids(value: number) {
        this._maxAsteroids = value;
    }

    public get maxAsteroids() {
        return this._maxAsteroids;
    }

    protected create(timestamp: number) {
        const size = random(this._maxSize, this._minSize, true);
        const a = new Asteroid({
            angle: random(360, 180, true),
            speed: random(this._maxSpeed, this._minSpeed),
            imageUrl: pickFromArray(this._images),
            mass: random(this._maxMass, this._minMass),
            size: { width: size, height: size },
            timestamp,
            position: {
                y: -size,
                x: random((this._canvas.width * 9) / 10, this._canvas.width / 10, true)
            }

        }, this._canvas.ctx);

        this._forces.forEach(f => a.addForce(f));
        this._gravityField && a.addGravityField(this._gravityField);
        return a;
    }

    public updatePositions(timestamp: number = 0) {

        this._items = this._items.filter(a => {
            return a.positionY > -a.height * 2 && a.positionY < this._canvas.height && a.positionX > -a.width && a.positionX < this._canvas.width;
        });

        this._items.forEach(a => a.updatePosition(timestamp));

        for (let i = 0; i < this._maxAsteroids - this._items.length; i++) {
            this._items.push(this.create(timestamp));
        }
    }

    public paint() {
        this._items.forEach(a => a.paint());
    }

    public remove(a: Asteroid): void;
    public remove(index: number): void;

    public remove(what: number | Asteroid) {
        if (what instanceof Asteroid) {
            what = this._items.indexOf(what);
        }
        this._items.splice(what, 1);
    }

    public clear() {
        this._items.length = 0;
        this._cycles = 0;
    }

}