import { INumericProjection, IVectorOptions, Vector } from './Vector';
import { AbstractParticle, IAbstractParticleOptions } from './AbstractParticle';
import { Canvas } from './Canvas';
import { random } from './utils';

// --

export interface IExplosionOptions {
    position: INumericProjection;
    minParticleSpeed?: number;
    maxParticleSpeed?: number;
    maxParticles: number;
    forces?: (IVectorOptions | Vector)[];
    timestamp: number;
    particleRadiusDecreaseRate?: number;
    onDone?: (e: Explosion) => void;
}

/**
 * Explosion
 */
export class Explosion {

    protected _particles: ExplosionParticle[] = [];
    protected _onDone: (e: Explosion) => void;

    constructor(options: IExplosionOptions, protected _canvas: Canvas) {
        this._onDone = options.onDone;
        const minParticleSpeed = isNaN(options.minParticleSpeed) ? 200 : options.minParticleSpeed;
        const maxParticleSpeed = isNaN(options.maxParticleSpeed) ? 200 : options.maxParticleSpeed;
        const radiusDecreaseRate = isNaN(options.particleRadiusDecreaseRate) ? .02 : options.particleRadiusDecreaseRate;
        

        for (let i = 0; i < options.maxParticles; i++) {
            const e = new ExplosionParticle({
                size: {
                    width: random(1, 0),
                    height: random(1, 0),
                },
                mass: random(10, 2),
                position: options.position,
                angle: random(360, 0, true),
                speed: random(maxParticleSpeed, minParticleSpeed),
                timestamp: options.timestamp,
                radiusDecreaseRate
            }, this._canvas.ctx);

            options.forces && options.forces.forEach(f => e.addForce(f));

            this._particles.push(e);
        }


    }

    public updatePositions(t: number) {
        this._particles.forEach((p, i) => {
            if (p.positionX > this._canvas.width || p.positionX < 0 || p.positionY > this._canvas.height || p.positionY < 0 || p.width <= 0) {
                this._particles.splice(i, 1);
            }
        });

        this._particles.forEach(p => p.updatePosition(t));


        if (this._particles.length === 0) this._onDone(this);
    }

    public paint() {
        this._canvas.ctx.globalCompositeOperation = "lighter";
        this._particles.forEach(p => p.paint());
    }
}

// --

export interface IExplosionParticle extends IAbstractParticleOptions {
    radiusDecreaseRate: number;
}


/**
 * ExplosionParticle
 */
export class ExplosionParticle extends AbstractParticle {

    protected _life = 0;
    protected _radiusDecreaseRate: number;

    constructor(options: IExplosionParticle, protected _ctx: CanvasRenderingContext2D) {
        super(options);
        this._radiusDecreaseRate = options.radiusDecreaseRate;
    }

    public updatePosition(t: number) {
        this._size.x -= this._radiusDecreaseRate;
        this._size.y -= this._radiusDecreaseRate;
        ++this._life;
        super.updatePosition(t);
    }

    public paint() {
        this._ctx.fillStyle = "rgba(" + (260 - (this._life * 2)) + "," + ((this._life * 2) + 50) + "," + (this._life * 2) + "," + 0.4 + ")";
        this._ctx.beginPath();
        this._ctx.arc(this.positionX, this.positionY, Math.abs(this.width), 0, 2 * Math.PI);
        this._ctx.fill();
    }
}