import { Movable, IMovableOptions, IPositionable } from './Movable';
import { INumericProjection } from './Vector';
import { GravityField } from './GravityField';

// --

export interface IAbstractParticleOptions extends IMovableOptions {
    size?: { width: number, height: number };
    mass?: number;
}

// --

export interface IParticle extends IPositionable {
    width: number;
    height: number;
}

/**
 * AbstractParticle
 */
export abstract class AbstractParticle extends Movable {

    protected _mass: number;
    protected _size: INumericProjection;
    protected _gravityFields: GravityField[];

    public collide(particle: IParticle): boolean {
        return this.positionY < particle.positionY + particle.height &&
            particle.positionY < this.positionY + this.height &&
            this.positionX < particle.positionX + particle.width &&
            particle.positionX < this.positionX + this.width;
    }

    constructor(options: IAbstractParticleOptions) {
        super(options);

        typeof options.size === 'object' && options.size || (options.size = { width: 10, height: 0 });
        this._mass = Number.isInteger(options.mass) && options.mass > 0 ? options.mass : 10;
        this._size = { x: Number.isInteger(options.size.width) ? options.size.width : 10, y: Number.isInteger(options.size.height) ? options.size.height : 10 };
        this._gravityFields = [];
    }

    public get mass() {
        return this._mass;
    }

    public get width() {
        return this._size.x;
    }

    public get height() {
        return this._size.y;
    }

    public addGravityField(field: GravityField) {
        this._gravityFields.push(field);
    }

    protected get resultant() {

        // override parent's implentation to account for gravitational fields

        const resultant = super.resultant;
        if (this._gravityFields.length === 0) return resultant;

        let g: INumericProjection;

        this._gravityFields.forEach(f => {
            g = f.getAcceleration(this);
            resultant.x += g.x;
            resultant.y += g.y;
        })

        return resultant;
    }

}