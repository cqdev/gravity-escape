import { Missile, IMissileOpts } from './Missile';
import { Rocket } from './Rocket';

// --

export interface IMissilePodOpts {
    imageUrl: string;
    count: number;
    life: number;
    fireInterval: number;
}

/**
 * MissilePod
 */
export class MissilePod {

    protected _missiles: Missile[] = [];
    protected _count: number;
    protected _fired: number;
    protected _maxLife: number;
    protected _fireInterval: number;
    public owner: Rocket = null;
    protected _lastFired: number = 0;
    protected _imageUrl: string;

    constructor(opts: IMissilePodOpts, protected _ctx: CanvasRenderingContext2D) {
        this._count = isNaN(opts.count) ? 99999 : opts.count;
        this._maxLife = isNaN(opts.life) ? 200 : opts.life;
        this._fireInterval = isNaN(opts.fireInterval) ? 2000 : opts.fireInterval;
        this._imageUrl = opts.imageUrl;

        this.reset();
    }

    public get missiles() {
        return this._missiles;
    }

    public reset() {
        this._fired = 0;
        this._lastFired = 0;
        this.clear();
    }

    public fire() {

        if (this.missilesLeft === 0 || this._fireInterval > this.owner.timestamp - this._lastFired) return;

        this._missiles.push(new Missile({
            imageUrl: this._imageUrl,
            angle: 90,
            speed: 1,
            mass: 15,
            position: {
                x: this.owner.positionX,
                y: this.owner.positionY + Math.round(this.owner.height / 2)
            },
            size: {
                height: 30,
                width: 10,
            },
            timestamp: this.owner.timestamp,
            forces: [
                {
                    acceleration: 100,
                    angle: 90,
                    name: 'missile-thrust'
                }
            ]
        }, this._ctx));

        this._lastFired = this.owner.timestamp;
        ++this._fired;
    }

    public get missilesLeft() {
        return this._count - this._fired;
    }

    public removeMissile(what: number): void;
    public removeMissile(what: Missile): void;

    public removeMissile(what: number | Missile) {
        if (what instanceof Missile) what = this._missiles.indexOf(what);
        this._missiles.splice(what, 1);
    }

    public updatePosition(timestamp: number) {
        for (let i = 0; i < this._missiles.length; i++) {
            if (this._missiles[i].life > this._maxLife) {
                console.log('Missile removed', this.missiles.length)
                this.removeMissile(i);
                i--;
            }
            else this._missiles[i].updatePosition(timestamp);
        }
    }

    public paint() {
        this._missiles.forEach(m => m.paint());
    }

    public clear() {
        this._missiles.length = 0;
    }
}