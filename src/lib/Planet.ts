
/**
 * Planet
 */
export class Planet {

    protected _element: HTMLImageElement;

    constructor(protected _imageUrl: string, protected _left: number, protected _top: number, protected _width: number, protected _height: number, protected _ctx: CanvasRenderingContext2D) {
        this._element = new Image();
        this._element.src = _imageUrl;
    }

    public paint() {
        if (this._width && this._height) this._ctx.drawImage(this._element, this._left, this._top, this._width, this._height);
        else this._ctx.drawImage(this._element, this._left, this._top);
    }
}