import { toRadians } from './utils';

// --

export interface IVectorOptions {
    name: string;
    acceleration: number;
    angle: number;
}

// --

export interface INumericProjection {
    x: number;
    y: number;
}

/**
 * Vector
 */
export class Vector {

    protected _name: string;
    protected _acceleration: INumericProjection;

    constructor(opts: IVectorOptions) {
        if (typeof opts.name !== 'string' || !opts.name) throw new TypeError('Invalid name');
        this._name = opts.name;
        this._acceleration = {
            x: Math.cos(toRadians(opts.angle)) * opts.acceleration,
            y: Math.sin(toRadians(opts.angle)) * opts.acceleration
        }
    }

    public get name() {
        return this._name;
    }

    public get accelerationX() {
        return this._acceleration.x;
    }

    public get accelerationY() {
        return this._acceleration.y;
    }

}